Dashboard project

The underground of this dashboard should be a microservice, which have a
REST interface with two endpoints. The first endpoint will be called by the checkout service
whenever a new payment is received and the second endpoint will provide statistics about the total
order amount and average amount per order for the last 60 seconds.
(Orders between t and t - 60 sec, given t = request time)

Installation:
- composer install
- yarn install
- php artisan migrate
- php artisan key:generate
- node server.js
