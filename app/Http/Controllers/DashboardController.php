<?php

namespace App\Http\Controllers;

use App\Events\StatEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;

class DashboardController extends Controller
{

    private $redisAmounts = [];
    private $redisEntries = [];

    public function __construct()
    {
        $this->middleware('auth');
        for($i = 0; $i<60; $i++){
            array_push($this->redisAmounts,'sales:'.sprintf("%02d", $i));
            array_push($this->redisEntries,'sales-count:'.sprintf("%02d", $i));
        }
    }

    /***
     * Dashboard page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('dashboard');
    }

    /***
     * Input page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function inputForm()
    {
        return view('input_form');
    }

    public function check()
    {
        return response()->json(['auth'=>Auth::check()]);
    }

    /***
     * Receiving statistics
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function sales(Request $request)
    {
        if($request->has('statistics')){
            $now = Carbon::now()->format('s');
            //firing event
            event(new StatEvent($request->input('statistics')));
            //saving amount of sales and number of entries per second
            Redis::incrbyfloat('sales:'.$now,$request->input('statistics'));
            Redis::expire('sales:'.$now,60);
            Redis::incrbyfloat('sales-count:'.$now,1);
            Redis::expire('sales-count:'.$now,60);
            return response('Accepted',202);
        }
        return response('Not accepted',501);
    }

    /***
     * Getting statistics
     * @return \Illuminate\Http\JsonResponse
     */
    public function statistics()
    {
        //getting amounts of sales per second and number of entries
        $totalAmount = Redis::mget($this->redisAmounts);
        $totalEntries = Redis::mget($this->redisEntries);
        //getting average value of sales
        $avg = array_sum($totalEntries)!=0 ? round(array_sum($totalAmount)/array_sum($totalEntries),2) : 0;
        return response()->json([
            'total_sales_amount'=>array_sum($totalAmount),
            'average_amount_per_order'=>$avg
        ]);
    }

    public function login(){
        if(Auth::attempt([
            'login' => request('login'),
            'password' => request('password')])){
            $user = Auth::user();
            $success['token'] = $user->createToken('Dashboard')->accessToken;
            return response()->json(['success' => $success], 200);
        }else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
}
