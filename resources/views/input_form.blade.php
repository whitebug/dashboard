@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="row m-2">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form-component></form-component>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
