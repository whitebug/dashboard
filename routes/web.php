<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('','DashboardController@index');
Route::get('input-form','DashboardController@inputForm');
Route::post('sales','DashboardController@sales');
Route::get('statistics','DashboardController@statistics');
Route::get('check','DashboardController@check');
