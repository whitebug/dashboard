var http = require('http').Server();
var io = require('socket.io')(http);
var Redis = require('ioredis');
var request = require('request');

var redis = new Redis();

io.use(function (socket, next) {
    request.get({
        url: 'http://dashboard.autoleader1.info/check',
        headers: {cookie : socket.request.headers.cookie},
        json:true
    }, function(error, response, json){
        console.log(json);
        return json.auth ? next() : next(new Error('Auth error'));
    });
});

redis.subscribe('statistics-channel');

redis.on('message', function (channel, message) {
    console.log('Message received: ' + message);
    console.log('Channel: ' + channel);
    message = JSON.parse(message);
    io.emit(channel + ':' + message.event, message.data);
});

http.listen(3322, function () {
    console.log('Listening on port: 3322');
});
